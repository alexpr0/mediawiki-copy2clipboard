function copy2clipboard(itemId) {
  var clipItem = document.getElementById(itemId);
  var clipText = document.createElement('textarea');
  clipText.value = clipItem.innerText;
  clipText.width = '1px';
  clipText.height = '1px';
  document.body.appendChild(clipText);
  clipText.select();
  document.execCommand('copy');
  document.body.removeChild(clipText);
}

<?php
/**
 * Copy2Clipboard - this extension lets you put item content to clipboard button
 *
 * To activate this extension, add the following into your LocalSettings.php file:
 * require_once('$IP/extensions/Copy2Clipboard/Copy2Clipboard.php');
 *
 * @ingroup Extensions
 * @author Alexander Pronin <alexander.pronin@gmail.com>
 * @link https://github.com/AlexanderPronin/MediaWiki-Copy2Clipboard
 */

if( !defined( 'MEDIAWIKI' ) ) {
    echo( "This is an extension to the MediaWiki package and cannot be run standalone.\n" );
    die( -1 );
}

$wgExtensionCredits['parser extensions'][] = array(
    'path'           => __FILE__,
    'name'           => 'Copy2Clipboard',
    'version'        => '0.1.0',
    'author'         => 'Alexander Pronin',
    'url'            => 'https://github.com/AlexanderPronin/MediaWiki-Copy2Clipboard',
    'descriptionmsg' => 'this extension lets you put item content to clipboard button',
);

$wgCopy2ClipboardPath = "$wgScriptPath/extensions/Copy2Clipboard";

$wgHooks['ParserFirstCallInit'][] = 'wfCopy2ClipboardInit';

function wfCopy2ClipboardInit( Parser $parser ) {
    global $wgOut, $wgJsMimeType, $wgCopy2ClipboardPath;

    $parser->setHook( 'copy2clipboard', 'wfCopy2ClipboardRender' );
    $wgOut->addScript("<script type=\"{$wgJsMimeType}\" src=\"$wgCopy2ClipboardPath/copy2clipboard.js\"></script>\n");

    return true;
}

function wfCopy2ClipboardRender( $input, array $args, Parser $parser, PPFrame $frame ) {
    global $wgCopy2ClipboardPath;

    if(!isset($args['item']))
        return '';

    $itemId = $args['item'];

    $html = "<button onClick=\"copy2clipboard('$itemId')\"><img src=\"$wgCopy2ClipboardPath/clipboard.png\" /> Copy to clipboard</button>";

    return $html;
}
